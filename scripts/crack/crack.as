
#include "weapons/weapon_sawedoff"
#include "weapons/weapon_clmp5"
#include "weapons/weapon_katana"
#include "weapons/weapon_rock"
#include "weapons/weapon_pepsigun"
#include "weapons/ShellEject"


array<ItemMapping@> g_ItemMappings = { 
	ItemMapping( "weapon_shotgun", SAWEDOFFName() )
};

void MapInit()
{	
	// Register custom weapons
	RegisterSAWEDOFF();
	RegisterCLMP5();
	RegisterKATANA();
	RegisterROCK();
	RegisterPEPSIGUN();

	
	
	// Initialize classic mode (item mapping only)
	g_ClassicMode.SetItemMappings( @g_ItemMappings );
	g_ClassicMode.ForceItemRemap( true );
}