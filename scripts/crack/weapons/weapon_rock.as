enum RockAnimation
{
	ROCK_IDLE,
	ROCK_THROW2,
	ROCK_DRAW
};

class weapon_rock : ScriptBasePlayerWeaponEntity
{
	private CBasePlayer@ m_pPlayer = null;
	int m_iSwing;
	float m_flStartThrow;
	TraceResult m_trHit;
	CBaseEntity@ pGrenade;
	float time;
	
	void Spawn()
	{
		self.Precache();
		g_EntityFuncs.SetModel( self, self.GetW_Model( "models/crack/w_rock.mdl") );
		self.m_iClip    	= -1;
		self.m_flCustomDmg	= self.pev.dmg;

		self.FallInit();
	}
	
	float WeaponTimeBase()
	{
		return g_Engine.time;
	}
	
	void Precache()
	{
		self.PrecacheCustomModels();

		g_Game.PrecacheModel( "models/crack/w_rock.mdl" );
		g_Game.PrecacheModel( "models/crack/v_rock.mdl" );
		g_Game.PrecacheModel( "models/crack/p_rock.mdl" );

		//Precache the Sprites as well
		g_Game.PrecacheModel( "sprites/cs16/640hud10.spr" );
		g_Game.PrecacheModel( "sprites/cs16/640hud11.spr" );
		
		g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_hit1.wav" );
        g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_hit2.wav" );
        g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_hit3.wav" );
        g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_hit4.wav" );
        g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_hitwall1.wav" );
		g_Game.PrecacheGeneric( "sound/" + "weapons/cs16/knife_deploy1.wav" );
		
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_hit1.wav" );
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_hit2.wav" );
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_hit3.wav" );
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_hit4.wav" );
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_hitwall1.wav" );
		g_SoundSystem.PrecacheSound( "weapons/cs16/knife_deploy1.wav" );
		
		g_Game.PrecacheGeneric( "sprites/" + "cs16/640hud10.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/640hud11.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/crosshairs.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/weapon_csknife.txt" );
	}
	
	bool GetItemInfo( ItemInfo& out info )
	{
		info.iMaxAmmo1		= -1;
		info.iMaxAmmo2		= -1;
		info.iMaxClip		= WEAPON_NOCLIP;
		info.iSlot			= 5;
		info.iPosition		= 5;
		info.iWeight		= 0;
		return true;
	}
	
	bool AddToPlayer( CBasePlayer@ pPlayer )
	{
		if( BaseClass.AddToPlayer ( pPlayer ) )
		{
			@m_pPlayer = pPlayer;
			NetworkMessage csknife( MSG_ONE, NetworkMessages::WeapPickup, pPlayer.edict() );
				csknife.WriteLong( g_ItemRegistry.GetIdForName("weapon_rock") );
			csknife.End();
			return true;
		}
	   
		return false;
	}
	
	bool Deploy()
	{
		bool bResult;
		{
			bResult = self.DefaultDeploy( self.GetV_Model( "models/crack/v_rock.mdl" ), self.GetP_Model( "models/crack/p_rock.mdl" ), ROCK_DRAW, "crowbar" );
			
			float deployTime = 1;
			self.m_flTimeWeaponIdle = self.m_flNextPrimaryAttack = self.m_flNextSecondaryAttack = g_Engine.time + deployTime;
			g_SoundSystem.EmitSound( m_pPlayer.edict(), CHAN_WEAPON, "weapons/cs16/knife_deploy1.wav", 1, ATTN_NORM );
			return bResult;
		}
	}
	
	void WeaponIdle()
	{
		if( self.m_flTimeWeaponIdle > g_Engine.time )
			return;
		
		self.SendWeaponAnim( ROCK_IDLE );
		self.m_flTimeWeaponIdle = g_Engine.time + 2;
	}
		
	
	void Holster( int skiplocal )
	{
		self.m_fInReload = false;// cancel any reload in progress.
		m_iSwing = 0;
		SetThink( null );

		m_pPlayer.m_flNextAttack = g_Engine.time + 0.5; 

		m_pPlayer.pev.viewmodel = string_t();
	}
	
	
	void PrimaryAttack()
	{
		Vector angThrow = m_pPlayer.pev.v_angle + m_pPlayer.pev.punchangle;

		if ( angThrow.x < 0 )
			angThrow.x = -2 + angThrow.x * ( ( 90 - 10 ) / 90.0 );
		else
			angThrow.x = -2 + angThrow.x * ( ( 90 + 10 ) / 90.0 );

		float flVel = ( 90.0f - angThrow.x ) * 15;

		if ( flVel > 3000.0f )
			flVel = 3000.0f;

		Math.MakeVectors ( angThrow );
		
		Vector vecSrc = m_pPlayer.pev.origin + m_pPlayer.pev.view_ofs + g_Engine.v_forward * 30;
		Vector vecThrow = g_Engine.v_forward * flVel + m_pPlayer.pev.velocity;
		
		time = m_flStartThrow - g_Engine.time + 9999999;
			if( time < 9999999 )
				time = 9999999;
		
		@pGrenade = g_EntityFuncs.ShootTimed( m_pPlayer.pev, vecSrc, vecThrow, time );
		g_EntityFuncs.SetModel( pGrenade, "models/crack/w_rock.mdl" );
		self.m_flNextPrimaryAttack = WeaponTimeBase() + 1;
		self.SendWeaponAnim( ROCK_THROW2 );
		m_pPlayer.SetAnimation( PLAYER_ATTACK1 ); 
		
	}
}



string GetROCKNAME()
{
	return "weapon_rock";
}

void RegisterROCK()
{
	g_CustomEntityFuncs.RegisterCustomEntity( GetROCKNAME(), GetROCKNAME() );
	g_ItemRegistry.RegisterWeapon( GetROCKNAME(), "cs16" );
}