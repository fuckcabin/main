enum katanaAnimation
{
	KATANA_IDLE1,
	KATANA_DRAW,
	KATANA_ATTACK1,
	KATANA_ATTACK1MISS,
	KATANA_ATTACK2,
	KATANA_ATTACK3,
	KATANA_ATTACK2HIT,
	KATANA_ATTACK3HIT

};

class weapon_katana : ScriptBasePlayerWeaponEntity
{
	private CBasePlayer@ m_pPlayer = null;
	int m_iSwing;
	float m_flStartThrow;
	TraceResult m_trHit;
	CBaseEntity@ pGrenade;
	float time;
	
	void Spawn()
	{
		self.Precache();
		g_EntityFuncs.SetModel( self, self.GetW_Model( "models/crack/w_katana.mdl") );
		self.m_iClip    	= -1;
		self.m_flCustomDmg	= self.pev.dmg;

		self.FallInit();
	}
	
	float WeaponTimeBase()
	{
		return g_Engine.time;
	}
	
	void Precache()
	{
		self.PrecacheCustomModels();

		g_Game.PrecacheModel( "models/crack/w_katana.mdl" );
		g_Game.PrecacheModel( "models/crack/v_katana.mdl" );
		g_Game.PrecacheModel( "models/crack/p_katana.mdl" );
		g_Game.PrecacheModel( "models/crack/fedora.mdl" );

		//Precache the Sprites as well
		g_Game.PrecacheModel( "sprites/cs16/640hud10.spr" );
		g_Game.PrecacheModel( "sprites/cs16/640hud11.spr" );
		
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_miss1.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_hit1.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_hitbod1.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_hitbod2.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_hitbod3.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_draw.wav" );
		g_SoundSystem.PrecacheSound( "crack/weapons/katana_tip.wav" );
		
		g_Game.PrecacheGeneric( "sprites/" + "cs16/640hud10.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/640hud11.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/crosshairs.spr" );
		g_Game.PrecacheGeneric( "sprites/" + "cs16/weapon_cskatana.txt" );
	}
	
	bool GetItemInfo( ItemInfo& out info )
	{
		info.iMaxAmmo1		= -1;
		info.iMaxAmmo2		= -1;
		info.iMaxClip		= WEAPON_NOCLIP;
		info.iSlot			= 0;
		info.iPosition		= 5;
		info.iWeight		= 0;
		return true;
	}
	
	bool AddToPlayer( CBasePlayer@ pPlayer )
	{
		if( BaseClass.AddToPlayer ( pPlayer ) )
		{
			@m_pPlayer = pPlayer;
			NetworkMessage cskatana( MSG_ONE, NetworkMessages::WeapPickup, pPlayer.edict() );
				cskatana.WriteLong( g_ItemRegistry.GetIdForName("weapon_katana") );
			cskatana.End();
			return true;
		}
	   
		return false;
	}
	
	bool Deploy()
	{
		bool bResult;
		{
			bResult = self.DefaultDeploy( self.GetV_Model( "models/crack/v_katana.mdl" ), self.GetP_Model( "models/crack/p_katana.mdl" ), KATANA_DRAW, "crowbar" );
			
			float deployTime = 1;
			self.m_flTimeWeaponIdle = self.m_flNextPrimaryAttack = g_Engine.time + deployTime;
			g_SoundSystem.EmitSound( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_draw.wav", 1, ATTN_NORM );
			return bResult;
		}
	}
	
	void WeaponIdle()
	{
		if( self.m_flTimeWeaponIdle > g_Engine.time )
			return;
		
		self.SendWeaponAnim( KATANA_IDLE1 );
		self.m_flTimeWeaponIdle = g_Engine.time + Math.RandomFloat( 10, 15 );
	}
		
	
	void Holster( int skiplocal )
	{
		self.m_fInReload = false;// cancel any reload in progress.
		m_iSwing = 0;
		SetThink( null );

		m_pPlayer.m_flNextAttack = g_Engine.time + 0.5; 

		m_pPlayer.pev.viewmodel = string_t();
	}
	
	void PrimaryAttack()
	{
		if( !Swing( 1 ) )
		{
			SetThink( ThinkFunction( this.SwingAgain ) );
			self.pev.nextthink = g_Engine.time + 0.01;
		}
	}
	
	void SecondaryAttack()
	{
		Vector angThrow = m_pPlayer.pev.v_angle + m_pPlayer.pev.punchangle;

		if ( angThrow.x < 0 )
			angThrow.x = -2 + angThrow.x * ( ( 90 - 10 ) / 90.0 );
		else
			angThrow.x = -2 + angThrow.x * ( ( 90 + 10 ) / 90.0 );

		float flVel = ( 90.0f - angThrow.x ) * 5;

		if ( flVel > 3000.0f )
			flVel = 3000.0f;

		Math.MakeVectors ( angThrow );
		
		Vector vecSrc = m_pPlayer.pev.origin + m_pPlayer.pev.view_ofs * 0;
		Vector vecThrow = g_Engine.v_forward * flVel + m_pPlayer.pev.velocity;
		
		time = m_flStartThrow - g_Engine.time + 9999999;
			if( time < 9999999 )
				time = 9999999;
		
		@pGrenade = g_EntityFuncs.ShootTimed( m_pPlayer.pev, vecSrc, vecThrow, time );
		g_EntityFuncs.SetModel( pGrenade, "models/crack/fedora.mdl" );
		self.m_flNextSecondaryAttack = WeaponTimeBase() + 30;
		g_SoundSystem.EmitSound( self.edict(), CHAN_ITEM, "crack/weapons/katana_tip.wav", 1, ATTN_NORM );
	}
	
	
	void Smack()
	{
		g_WeaponFuncs.DecalGunshot( m_trHit, BULLET_PLAYER_CROWBAR );
	}
	
	void SwingAgain()
	{
		Swing( 0 );
	}
	
	
	bool Swing( int fFirst )
	{
		bool fDidHit = false;

		TraceResult tr;

		Math.MakeVectors( m_pPlayer.pev.v_angle );
		Vector vecSrc	= m_pPlayer.GetGunPosition();
		Vector vecEnd	= vecSrc + g_Engine.v_forward * 47;

		g_Utility.TraceLine( vecSrc, vecEnd, dont_ignore_monsters, m_pPlayer.edict(), tr );

		if ( tr.flFraction >= 1.0 )
		{
			g_Utility.TraceHull( vecSrc, vecEnd, dont_ignore_monsters, head_hull, m_pPlayer.edict(), tr );
			if ( tr.flFraction < 1.0 )
			{
				// Calculate the point of intersection of the line (or hull) and the object we hit
				// This is and approximation of the "best" intersection
				CBaseEntity@ pHit = g_EntityFuncs.Instance( tr.pHit );
				if ( pHit is null || pHit.IsBSPModel() )
					g_Utility.FindHullIntersection( vecSrc, tr, tr, VEC_DUCK_HULL_MIN, VEC_DUCK_HULL_MAX, m_pPlayer.edict() );
				vecEnd = tr.vecEndPos;	// This is the point on the actual surface (the hull could have hit space)
			}
		}

		if ( tr.flFraction >= 1.0 )
		{
			if( fFirst != 0 )
			{
				// miss
				switch( ( m_iSwing++ ) % 2 )
				{
				case 0:
					self.SendWeaponAnim( KATANA_ATTACK3 ); break;
				case 1:
					self.SendWeaponAnim( KATANA_ATTACK2 ); break;
				}
				self.m_flNextPrimaryAttack = g_Engine.time + 0.6;
				// play wiff or swish sound
				g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_miss1.wav", 1, ATTN_NORM, 0, 94 + Math.RandomLong( 0,0xF ) );

				// player "shoot" animation
				m_pPlayer.SetAnimation( PLAYER_ATTACK1 ); 
			}
		}
		else
		{
			// hit
			fDidHit = true;
			
			CBaseEntity@ pEntity = g_EntityFuncs.Instance( tr.pHit );

			switch( ( ( m_iSwing++ ) % 2 ) )
			{
			case 0:
				self.SendWeaponAnim( KATANA_ATTACK2HIT ); break;
			case 1:
				self.SendWeaponAnim( KATANA_ATTACK3HIT ); break;

			}
			
			if( self.m_flNextSecondaryAttack > WeaponTimeBase() + 5 )
			{	self.m_flNextSecondaryAttack = WeaponTimeBase() + 5;
			}
			
			m_pPlayer.SetAnimation( PLAYER_ATTACK1 );

				float flDamage = 85;
			if ( self.m_flCustomDmg > 0 )
				flDamage = self.m_flCustomDmg;	

			g_WeaponFuncs.ClearMultiDamage();
			if ( self.m_flNextPrimaryAttack < g_Engine.time )
			{
				// first swing does full damage
				pEntity.TraceAttack( m_pPlayer.pev, flDamage, g_Engine.v_forward, tr, DMG_CLUB );  
			}
			g_WeaponFuncs.ApplyMultiDamage( m_pPlayer.pev, m_pPlayer.pev );				

			//m_flNextPrimaryAttack = gpGlobals->time + 0.30; //0.25

			// play thwack, smack, or dong sound
			float flVol = 1.0;
			bool fHitWorld = true;

			if( pEntity !is null )
			{
				self.m_flNextPrimaryAttack = g_Engine.time + 0.6; //0.25

				if( pEntity.Classify() != CLASS_NONE && pEntity.Classify() != CLASS_MACHINE && pEntity.BloodColor() != DONT_BLEED )
				{
	// aone
					if( pEntity.IsPlayer() )		// lets pull them
					{
						pEntity.pev.velocity = pEntity.pev.velocity + ( self.pev.origin - pEntity.pev.origin ).Normalize() * 120;
					}
	// end aone
					// play thwack or smack sound
					switch( Math.RandomLong( 0, 2 ) )
					{
					case 0:
						g_SoundSystem.EmitSound( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_hitbod1.wav", 1, ATTN_NORM ); break;
					case 1:
						g_SoundSystem.EmitSound( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_hitbod2.wav", 1, ATTN_NORM ); break;
					case 2:
						g_SoundSystem.EmitSound( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_hitbod3.wav", 1, ATTN_NORM ); break;
					}
					m_pPlayer.m_iWeaponVolume = 128; 
					if( !pEntity.IsAlive() )
						return true;
					else
						flVol = 0.1;

					fHitWorld = false;
				}
			}

			// play texture hit sound
			// UNDONE: Calculate the correct point of intersection when we hit with the hull instead of the line

			if( fHitWorld == true )
			{
				float fvolbar = g_SoundSystem.PlayHitSound( tr, vecSrc, vecSrc + ( vecEnd - vecSrc ) * 2, BULLET_PLAYER_CROWBAR );
				
				self.m_flNextPrimaryAttack = g_Engine.time + 0.6; //0.25
				
				// override the volume here, cause we don't play texture sounds in multiplayer, 
				// and fvolbar is going to be 0 from the above call.

				fvolbar = 1;

				// also play crowbar strike
				
				g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_WEAPON, "crack/weapons/katana_hit1.wav", fvolbar, ATTN_NORM, 0, 98 + Math.RandomLong( 0, 3 ) ); 
			}

			// delay the decal a bit
			m_trHit = tr;
			SetThink( ThinkFunction( this.Smack ) );
			self.pev.nextthink = g_Engine.time + 0.2;

			m_pPlayer.m_iWeaponVolume = int( flVol * 512 ); 
		}
		return fDidHit;
	}
}

string GetKATANAName()
{
	return "weapon_katana";
}

void RegisterKATANA()
{
	g_CustomEntityFuncs.RegisterCustomEntity( GetKATANAName(), GetKATANAName() );
	g_ItemRegistry.RegisterWeapon( GetKATANAName(), "cs16" );
}