

const int PEPSIGUN_DEFAULT_AMMO = 100;
const int PEPSIGUN_MAX_CLIP 	= 8;
const int PEPSIGUN_WEIGHT		= 15;
const int HANDGRENADE_MAX_CARRY = 60;


enum ShotgunAnimation
{
	PEPSIGUN_IDLE,
	PEPSIGUN_FIRE,
	PEPSIGUN_INSERT,
	PEPSIGUN_CLOSE,
	PEPSIGUN_OPEN,
	PEPSIGUN_DRAW
};

class weapon_pepsigun : ScriptBasePlayerWeaponEntity
{
	private CBasePlayer@ m_pPlayer = null;
	
	float m_flNextReload;
	int m_iShell;
	float m_flPumpTime;
	bool m_fPlayPumpSound;
	bool m_fShotgunReload;
	float m_flStartThrow;
	CBaseEntity@ pGrenade;
	float time;

	void Spawn()
	{
		Precache();
		g_EntityFuncs.SetModel( self, "models/crack/w_pepsigun.mdl" );
		
		self.m_iDefaultAmmo = PEPSIGUN_DEFAULT_AMMO;

		self.FallInit();// get ready to fall
	}

	void Precache()
	{
		self.PrecacheCustomModels();
		g_Game.PrecacheModel( "models/crack/v_pepsigun.mdl" );
		g_Game.PrecacheModel( "models/crack/w_pepsigun.mdl" );
		g_Game.PrecacheModel( "models/crack/p_pepsigun.mdl" );
		g_Game.PrecacheModel( "models/crack/w_grenade.mdl" );

		m_iShell = g_Game.PrecacheModel( "models/shotgunshell.mdl" );

		g_SoundSystem.PrecacheSound( "items/9mmclip1.wav" );              

		g_SoundSystem.PrecacheSound( "hl/weapons/dbarrel1.wav" );
		g_SoundSystem.PrecacheSound( "hl/weapons/sbarrel1.wav" );

		g_SoundSystem.PrecacheSound( "hl/weapons/reload1.wav" );
		g_SoundSystem.PrecacheSound( "hl/weapons/reload3.wav" );

		g_SoundSystem.PrecacheSound("weapons/sshell1.wav");
		g_SoundSystem.PrecacheSound("weapons/sshell3.wav");
		
		g_SoundSystem.PrecacheSound( "hl/weapons/357_cock1.wav" );
		g_SoundSystem.PrecacheSound( "hl/weapons/scock1.wav" );
	}

	bool AddToPlayer( CBasePlayer@ pPlayer )
	{
		if ( !BaseClass.AddToPlayer( pPlayer ) )
			return false;
			
		@m_pPlayer = pPlayer;
		
		NetworkMessage message( MSG_ONE, NetworkMessages::WeapPickup, pPlayer.edict() );
			message.WriteLong( self.m_iId );
		message.End();
		
		return true;
	}
	
	bool PlayEmptySound()
	{
		if( self.m_bPlayEmptySound )
		{
			self.m_bPlayEmptySound = false;
			
			g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_WEAPON, "hl/weapons/357_cock1.wav", 0.8, ATTN_NORM, 0, PITCH_NORM );
		}
		
		return false;
	}

	bool GetItemInfo( ItemInfo& out info )
	{
		info.iMaxAmmo1 	= HANDGRENADE_MAX_CARRY;
		info.iMaxAmmo2 	= -1;
		info.iMaxClip 	= PEPSIGUN_MAX_CLIP;
		info.iSlot 		= 4;
		info.iPosition 	= 4;
		info.iFlags 	= 0;
		info.iWeight 	= PEPSIGUN_WEIGHT;

		return true;
	}

	bool Deploy()
	{
		return self.DefaultDeploy( self.GetV_Model( "models/crack/v_pepsigun.mdl" ), self.GetP_Model( "models/crack/p_pepsigun.mdl" ), PEPSIGUN_DRAW, "shotgun" );
	}

	float WeaponTimeBase()
	{
		return g_Engine.time;
	}
	
	void Holster( int skipLocal = 0 )
	{
		m_fShotgunReload = false;
		
		BaseClass.Holster( skipLocal );
	}


	
	void PrimaryAttack()
	{
		if( m_pPlayer.pev.waterlevel == WATERLEVEL_HEAD || self.m_iClip <= 0 )
		{
			self.PlayEmptySound();
			self.m_flNextPrimaryAttack = WeaponTimeBase() + 0.15f;
			return;
		}
		Vector angThrow = m_pPlayer.pev.v_angle + m_pPlayer.pev.punchangle;

		if ( angThrow.x < 0 )
			angThrow.x = -2 + angThrow.x * ( ( 90 - 10 ) / 90.0 );
		else
			angThrow.x = -2 + angThrow.x * ( ( 90 + 10 ) / 90.0 );

		float flVel = ( 90.0f - angThrow.x ) * 7;

		if ( flVel > 750.0f )
			flVel = 750.0f;

		Math.MakeVectors ( angThrow );
		
		Vector vecSrc = m_pPlayer.pev.origin + m_pPlayer.pev.view_ofs + g_Engine.v_forward * 30;
		Vector vecThrow = g_Engine.v_forward * flVel + m_pPlayer.pev.velocity;
		
		time = m_flStartThrow - g_Engine.time + 3;
			if( time < 3 )
				time = 3;
		
		@pGrenade = g_EntityFuncs.ShootTimed( m_pPlayer.pev, vecSrc, vecThrow, time );
		g_EntityFuncs.SetModel( pGrenade, "models/crack/w_grenade.mdl" );
		self.m_flNextPrimaryAttack = WeaponTimeBase() + 0.8;
		self.SendWeaponAnim( PEPSIGUN_FIRE );
		--self.m_iClip;
		m_pPlayer.SetAnimation( PLAYER_ATTACK1 ); 
		
	}


	void Reload()
	{
		if( m_pPlayer.m_rgAmmo( self.m_iPrimaryAmmoType ) <= 0 || self.m_iClip == PEPSIGUN_MAX_CLIP )
			return;

		if( m_flNextReload > g_Engine.time )
			return;

		// don't reload until recoil is done
		if( self.m_flNextPrimaryAttack > g_Engine.time && !m_fShotgunReload )
			return;

		// check to see if we're ready to reload
		if( !m_fShotgunReload )
		{
			self.SendWeaponAnim( PEPSIGUN_INSERT, 0, 0 );
			m_pPlayer.m_flNextAttack 	= 0.6;	//Always uses a relative time due to prediction
			self.m_flTimeWeaponIdle			= g_Engine.time + 0.6;
			self.m_flNextPrimaryAttack 		= g_Engine.time + 1.0;
			self.m_flNextSecondaryAttack	= g_Engine.time + 1.0;
			m_fShotgunReload = true;
			return;
		}
		else if( m_fShotgunReload )
		{
			if( self.m_flTimeWeaponIdle > g_Engine.time )
				return;

			if( self.m_iClip == PEPSIGUN_MAX_CLIP )
			{
				m_fShotgunReload = false;
				return;
			}

			self.SendWeaponAnim( PEPSIGUN_CLOSE, 0 );
			m_flNextReload 					= g_Engine.time + 0.5;
			self.m_flNextPrimaryAttack 		= g_Engine.time + 0.5;
			self.m_flNextSecondaryAttack 	= g_Engine.time + 0.5;
			self.m_flTimeWeaponIdle 		= g_Engine.time + 0.5;
				
			// Add them to the clip
			self.m_iClip += 1;
			m_pPlayer.m_rgAmmo( self.m_iPrimaryAmmoType, m_pPlayer.m_rgAmmo( self.m_iPrimaryAmmoType ) - 1 );
			
			switch( Math.RandomLong( 0, 1 ) )
			{
			case 0:
				g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_ITEM, "hl/weapons/reload1.wav", 1, ATTN_NORM, 0, 85 + Math.RandomLong( 0, 0x1f ) );
				break;
			case 1:
				g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_ITEM, "hl/weapons/reload3.wav", 1, ATTN_NORM, 0, 85 + Math.RandomLong( 0, 0x1f ) );
				break;
			}
		}

		BaseClass.Reload();
	}

	void WeaponIdle()
	{
		self.ResetEmptySound();

		m_pPlayer.GetAutoaimVector( AUTOAIM_5DEGREES );

		if( self.m_flTimeWeaponIdle < g_Engine.time )
		{
			if( self.m_iClip == 0 && !m_fShotgunReload && m_pPlayer.m_rgAmmo( self.m_iPrimaryAmmoType ) != 0 )
			{
				self.Reload();
			}
			else if( m_fShotgunReload )
			{
				if( self.m_iClip != PEPSIGUN_MAX_CLIP && m_pPlayer.m_rgAmmo( self.m_iPrimaryAmmoType ) > 0 )
				{
					self.Reload();
				}
				else
				{
					// reload debounce has timed out
					self.SendWeaponAnim( PEPSIGUN_OPEN, 0, 0 );

					g_SoundSystem.EmitSoundDyn( m_pPlayer.edict(), CHAN_ITEM, "hl/weapons/scock1.wav", 1, ATTN_NORM, 0, 95 + Math.RandomLong( 0,0x1f ) );
					m_fShotgunReload = false;
					self.m_flTimeWeaponIdle = g_Engine.time + 1.5;
				}
			}
			else
			{
				int iAnim;
				switch( g_PlayerFuncs.SharedRandomLong( m_pPlayer.random_seed, 0, 2 ) )
				{
					case 0:
					iAnim = PEPSIGUN_IDLE;
					self.m_flTimeWeaponIdle = WeaponTimeBase() + (60.0/12.0);
					break;

					case 1:
					iAnim = PEPSIGUN_IDLE;
					self.m_flTimeWeaponIdle = WeaponTimeBase() + (20.0/9.0);
					break;

					case 2:
					iAnim = PEPSIGUN_IDLE;
					self.m_flTimeWeaponIdle = WeaponTimeBase() + (20.0/9.0);
					break;
				}

				self.SendWeaponAnim( iAnim, 0, 0 );
			}
		}
	}
}

string GetPEPSIGUNNAME()
{
	return "weapon_pepsigun";
}

void RegisterPEPSIGUN()
{
	g_CustomEntityFuncs.RegisterCustomEntity( "weapon_pepsigun", GetPEPSIGUNNAME() );
	g_ItemRegistry.RegisterWeapon( GetPEPSIGUNNAME(), "hl_weapons", "Hand Grenade" );
}
